from tkinter import*
import math


canvasWidth = 1000
canvasHeight = 470


penDown = True
x = 0
y = 0
vx = 1
vy = 0
line = 0
colorPen = "black"
colorBackground = "white"


def changeColorPen(value: str):
    global colorPen
    colorPen = value


def changeColorBackground(value: str):
    global colorBackground
    colorBackground = value
    canvas.configure(bg= colorBackground)


def doPenUp():
    global penDown
    penDown = False


def doPenDown():
    global penDown
    penDown = True


def move(move):
    global x, y, line
    move = float(move)
    xf = x + move * vx
    yf = y + move * vy

    line = line + 1

    if penDown == True:
        canvas.create_line(x + canvasWidth / 2, y + canvasHeight / 2, xf + canvasWidth / 2,
                           yf + canvasHeight / 2, fill=colorPen, width=2, tags=line)
    x = xf
    y = yf

    canvas.coords(img, x + canvasWidth / 2, y + canvasHeight / 2)
    canvas.tag_raise(img)


def turn(angle: float):
    global vx, vy, vxf, vyf
    alpha = angle * math.pi / 180.0
    vxf = vx * math.cos(alpha) - vy * math.sin(alpha)
    vyf = vx * math.sin(alpha) + vy * math.cos(alpha)
    vx = vxf
    vy = vyf


def testCode(command, value):
    if command == "av" or command == "avance":
        value = float(value)
        move(value)
    elif command == "re" or command == "recule":
        value = float(value)
        move(-value)
    elif command == "td" or command == "tournedroite":
        value = float(value)
        turn(value)
    elif command == "tg" or command == "tournegauche":
        value = float(value)
        turn(-value)
    elif command == "lc" or command == "levecrayon":
        doPenUp()
    elif command == "bc" or command == "baissecrayon":
        doPenDown()
    elif command == "cc" or command == "couleurcrayon":
        changeColorPen(value)
    elif command == "cf" or command == "couleurfond":
        changeColorBackground(value)


def parseLine(line: str):
    items = line.split()
    command = items[0].lower()
    value = None
    if len(items) == 2:
        value = items[1]

    entryCode.delete(0, 'end')
    testCode(command, value)


def executeCode():
    line = entryCode.get()
    code.insert(0, line)
    parseLine(line)


def delete():
    # code.delete(0, 0)
    pass

toolkit = Tk()
toolkit.title("Tortue Logo")
toolkit.geometry("1050x1050")


# Création du canevas
canvas = Canvas(toolkit, width=canvasWidth, height=canvasHeight,
                borderwidth=2, relief="solid", background = colorBackground)
canvas.pack()
# entrée

tortue = PhotoImage(file="tortue.gif")
img = canvas.create_image(x + canvasWidth / 2, y + canvasHeight / 2, image=tortue)

entryCode = Entry(toolkit, textvariable="", width=20,)
entryCode.place(x=200, y=500)

scrollbar = Scrollbar(toolkit)

code = Listbox(toolkit, height=15, yscrollcommand=scrollbar.set)
code.place(x=20, y=500)

scrollbar.place(x=5, y=500)
scrollbar.config(command=code.yview)

ok = Button(text="executer", width=17, height=9, command=executeCode)
ok.place(x=200, y=530)

supprimer = Button(text="supprimer", width=17, height=3, command=delete)
supprimer.place(x=200, y=705)

aideText = '''av n  ou  avance n  :  La tortue avance de n pas 
\nre n  ou  recule n  :  La tortue recule de n pas
\ntd n  ou  tournedroite n  :  La tortue tourne de n degrés d'angle vers la droite
\ntg n  ou  tournegauche n  :  La tortue tourne de n degrés d'angle vers la gauche
\nlc  ou  levecrayon  :  La tortue ne laisse pas de trace
\nbc ou baissecrayon  :  La tortue laisse sa trace (par défaut)
\ncc n ou couleurcrayon n  :  Change la couleur du crayon,
\nn est une couleur écrite en anglai*
\ncf n ou couleurfond n  :  Change la couleur du fond,
\nn est une couleur écrite en anglai*
\n*Les couleur disponible sont :
\n"black" (Noir), "red" (Rouge), "green" (Vert), "yellow" (Jaune),
\n"blue" (Bleu), "magenta" (Fushia), "cyan" (Cyan), "white" (Blanc).'''

aide = Label(toolkit, text=aideText, justify="left")
aide.place(x=370, y=500)


toolkit.mainloop()
